## Phaser Demo

[Phaser](http://phaser.io/) is a fun library used to create games with JavaScript.
This example shows a basic set up with stages, adding sprites, text, and animations.

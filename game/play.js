// this is the main stage of the game
// it contains all the logic of movements and interactions
// it will initialize the lose or win stage
var playState = {

  create: function() {
    // adds sprite to the game
    game.add.sprite(0,0,'sky').scale.setTo(1.5,1.5);
    // creates platform
    platforms = game.add.group();
    platforms.enableBody = true;
    // creates ground
    var ground = platforms.create(0, game.world.height-20, 'platform');
    ground.scale.setTo(2, 1);
    ground.body.immovable = true;
    // creates ledge
    var ledge = platforms.create(550, 100, 'platform');
    ledge.scale.setTo(.3,0.3);
    ledge.body.immovable = true;

    // creates player and adds physics to it
    player = game.add.sprite(10, game.world.height -200, 'llama');
    game.physics.arcade.enable(player);
    player.body.bounce.y = 0.2;
    player.body.gravity.y = 800;
    player.body.collideWorldBounds = true;

    // adds animation from the sprites
    player.animations.add('left', [0, 1, 2, 3], 10, true);
    player.animations.add('right', [0, 1, 2, 3], 10, true);

    // adds cursor keys
    cursors = game.input.keyboard.createCursorKeys();
    // adds logo
    logos = game.add.group();
    logos.enableBody = true;
    var logo = logos.create(600, 0, 'logo');
    logo.scale.setTo(.5,.5);
    logo.body.gravity.y = 4;
    // adds score text on the top left
    scoreText = game.add.text(16, 16, 'get to galvanize!', { fontSize: '20px', fill: 'green' });
  },

  update: function() {
    // adds colition between the different sprites of the game
    game.physics.arcade.collide(player, platforms);
    game.physics.arcade.collide(logos, platforms);
    game.physics.arcade.overlap(player, logos, this.win, null, this);
    player.body.velocity.x = 0;
    // adds game controls
    if (cursors.left.isDown)
    {
        player.body.velocity.x = -300;
        player.animations.play('left');
    }
    else if (cursors.right.isDown)
    {
        player.body.velocity.x = 300;
        player.animations.play('right');
    }
    else
    {
        player.animations.stop();
        player.frame = 0;
    }
    if (cursors.up.isDown && player.body.touching.down)
    {
        player.body.velocity.y = -750;
    }
  },
  // when the player collides with the logo
  collectLogo: function(player, logo) {
    logo.kill();
    logostatus++;
    scoreText.text = 'logos : ' + logostatus;
    if (logostatus === 3) {
      this.win();
    }
  },
  // runs the gameOver stage when invoked
  gameOver: function() {
    game.state.start('lose');
  },
  // runs the win stage when invoked
  win: function() {
    game.state.start('win');
  },
}

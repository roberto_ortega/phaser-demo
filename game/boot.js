// booting stage that initializes physics
// then starts the load stage
var bootState = {
  create : function () {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.state.start('load');
  },
}

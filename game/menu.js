// loads front page seen by the user
var menuState = {

  create: function () {
    var winLabel = game.add.text(80,100, 'Hit enter to play', {font: '50px Courier', fill: 'orange'});
    // when the enter key is pressed, the play phase is initilized
    enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    enterKey.onDown.addOnce(this.start, this);
  },

  start: function () {
    game.state.start('play');
  },

}

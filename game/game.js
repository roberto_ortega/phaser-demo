// creates the game from the <div class="game"></div>
var game = new Phaser.Game(800, 600, Phaser.AUTO, '.game');

// initialize global variables to be used throughout the game
var logostatus = 0;
var scoreText;
var platform;

// boot up all the different stages, one by one
// look inside each file inside the game directory
game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);
game.state.add('win', winState);
game.state.add('lose', loseState);

// initiate the game after all the stages are loaded
game.state.start('boot');
